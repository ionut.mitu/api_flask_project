# API_Flask_project

An Restful Api project developed with Flask. 
I used Prometheus and Grafana for monitoring and containerization with Docker.


To run the project you need docker-compose and run this command:
docker-compose up -d

To stop:
docker-compose down

Routes
* "/" - show informations about what routes are avaible
* "/fibonacci/<int>" - the n-th fibbonaci number
* "/power/<float>/<float>" - show power of first number to second number
* "/factorial/<int>" - show the factorial of a number
* "/get_requests" - show all requests saved in DB
* "/metric" - show metrics

Test using curl:
* curl -i -X GET "http://localhost:5000/"
* curl -i -X GET "http://localhost:5000/power/10/10"
* curl -i -X GET "http://localhost:5000/factorial/10"
* curl -i -X GET "http://localhost:5000/fibonacci/10"
* curl -i -X GET "http://localhost:5000/get_requests"

On port 9090 is a instance of Prometheus that collect metrics from the Flask app. 
On port 3000 is an instance of Grafana that displays the data collected by Prometheus (user: admin, pass: admin)

To see the Grafana dashboard:

http://localhost:3000/grafana/d/_eX4mpl3/example-dashboard?orgId=1&refresh=5s 
