FROM python:3.6.6-slim

#Install missing required packages
RUN apt update && \
apt install --yes libldap2-dev libsasl2-dev && \
apt install --yes gcc && \
apt install --yes vim

RUN mkdir -p /endava-project
WORKDIR /endava-project

COPY . /endava-project

# Install requirements and upgrade pip package
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

CMD ["python", "app.py"]